package com.clevertec_mobilelab_2022_alex_lukanev.task2

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.viewModels
import com.clevertec_mobilelab_2022_alex_lukanev.task2.databinding.FragmentMainBinding
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.MainViewModel
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.Person
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.ItemClickListener
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.Navigator
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.adapter.PersonsAdapter


class MainFragment : Fragment(), ItemClickListener {

    private lateinit var binding: FragmentMainBinding
    private val viewModel: MainViewModel by viewModels()
    private val adapter: PersonsAdapter get() = views { (personsList.adapter as? PersonsAdapter)!! }
    private var navigator: Navigator? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        val persons: List<Person> = viewModel.getPersonsList()
        views {
            personsList.adapter = PersonsAdapter(this@MainFragment)
            adapter.submitList(persons)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        postponeEnterTransition()
        (view.parent as? ViewGroup)?.doOnPreDraw {
            startPostponedEnterTransition()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = if (context is Navigator) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                        + " must implement Navigator"
            )
        }
    }

    private fun <T : Any> views(block: FragmentMainBinding.() -> T): T = binding.block()

    override fun onItemClickListener(id: Int, sharedImageView: ImageView) {
        navigator?.onPersonFragment(id, sharedImageView)

    }
}