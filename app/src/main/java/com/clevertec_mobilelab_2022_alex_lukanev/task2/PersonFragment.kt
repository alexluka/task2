package com.clevertec_mobilelab_2022_alex_lukanev.task2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.transition.TransitionInflater
import com.clevertec_mobilelab_2022_alex_lukanev.task2.databinding.FragmentPersonBinding
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.Person
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.PersonViewModel
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.Navigator


private const val ID = "id"

class PersonFragment : Fragment() {
    private var navigator: Navigator? = null
    private var id: Int? = null
    private lateinit var binding: FragmentPersonBinding
    private val viewModel: PersonViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
            sharedElementEnterTransition = TransitionInflater.from(requireContext()).inflateTransition(R.transition.shared_image)
        arguments?.let {
            id = it.getInt(ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val person: Person = viewModel.getPerson(id!!)
        binding = FragmentPersonBinding.inflate(inflater, container, false)
        views {
            ViewCompat.setTransitionName(personIV, person.id.toString())
            titleTV.text = person.title
            descriptionTV.text = person.description
            exitButton.setOnClickListener {
                navigator?.onExit()
            }
            toolbar.setNavigationOnClickListener {
                navigator?.onMainFragment()
            }
        }
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = if (context is Navigator) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                        + " must implement Navigator"
            )
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(id: Int) =
            PersonFragment().apply {
                arguments = Bundle().apply {
                    putInt(ID, id)
                }
            }
    }

    private fun <T : Any> views(block: FragmentPersonBinding.() -> T): T = binding.block()
}