package com.clevertec_mobilelab_2022_alex_lukanev.task2.model

object PersonsService {
    val persons: List<Person> = (1..1000).map {
        Person(
            id = it,
            title = "Title $it",
            description = "Description $it"
        )
    }
}