package com.clevertec_mobilelab_2022_alex_lukanev.task2.model

import androidx.lifecycle.ViewModel

class PersonViewModel : ViewModel() {
    fun getPerson(id: Int) : Person = PersonsService.persons[id]
}
