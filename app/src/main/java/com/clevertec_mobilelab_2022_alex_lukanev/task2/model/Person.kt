package com.clevertec_mobilelab_2022_alex_lukanev.task2.model

data class Person(
    val id: Int,
    val title: String,
    val description: String
)
