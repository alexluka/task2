package com.clevertec_mobilelab_2022_alex_lukanev.task2.model

import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
    fun getPersonsList() : List<Person> = PersonsService.persons
}