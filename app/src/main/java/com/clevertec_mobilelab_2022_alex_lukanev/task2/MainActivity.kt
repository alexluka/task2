package com.clevertec_mobilelab_2022_alex_lukanev.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.*

class MainActivity : AppCompatActivity(), Navigator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, MainFragment())
                .commit()
        }
    }

    override fun onPersonFragment(id: Int, sharedImageView: ImageView) {
        val fragment: Fragment = PersonFragment.newInstance(id)
            supportFragmentManager
                .beginTransaction()
                .setReorderingAllowed(true)
                .addSharedElement(sharedImageView, ViewCompat.getTransitionName(sharedImageView).toString())
                .addToBackStack(null)
                .replace(R.id.container, fragment)
                .commit()
    }

    override fun onMainFragment() {
        onBackPressed()
    }

    override fun onExit() {
        finish()
    }

}



