package com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.clevertec_mobilelab_2022_alex_lukanev.task2.databinding.PersonItemBinding
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.Person

class PersonViewHolder(
    private val binding: PersonItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    private var item: Person? = null


    fun onBind(item: Person) {
        this.item = item
        views {
            titleTextView.text = item.title
            descriptionTextView.text = item.description
        }
    }

    fun getImageView() = binding.personImageView

    private fun <T> views(block: PersonItemBinding.() -> T): T? = binding.block()

    companion object {
        fun create(parent: ViewGroup) = PersonItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ).let(::PersonViewHolder)
    }
}