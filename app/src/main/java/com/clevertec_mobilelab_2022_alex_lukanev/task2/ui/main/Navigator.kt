package com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main

import android.widget.ImageView

interface Navigator {
    fun onPersonFragment(id: Int, sharedImageView: ImageView)
    fun onMainFragment()
    fun onExit()
}

interface ItemClickListener {
    fun onItemClickListener(id: Int, sharedImageView: ImageView)
}