package com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.Person

class PersonDiffCallBack : DiffUtil.ItemCallback<Person>() {
    override fun areItemsTheSame(oldItem: Person, newItem: Person): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Person, newItem: Person): Boolean = oldItem == newItem
}