package com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.adapter

import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.ListAdapter
import com.clevertec_mobilelab_2022_alex_lukanev.task2.model.Person
import com.clevertec_mobilelab_2022_alex_lukanev.task2.ui.main.ItemClickListener

class PersonsAdapter(private val itemClickListener: ItemClickListener) :
    ListAdapter<Person, PersonViewHolder>(PersonDiffCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder =
        PersonViewHolder.create(parent)


    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        holder.onBind(getItem(position))
        ViewCompat.setTransitionName(holder.getImageView(), (position + 1).toString())
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClickListener(position, holder.getImageView())
        }
    }
}
